<?php
namespace tweeterapp\view;

class TweeterView extends \mf\view\AbstractView {
  
    /* Constructeur 
    *
    * Appelle le constructeur de la classe parent
    */
    public function __construct( $data ){
        parent::__construct($data);
    }

    /* MÃ©thode renderHeader
     *
     *  Retourne le fragment HTML de l'entÃªte (unique pour toutes les vues)
     */ 
    private function renderHeader(){
        return '<h1>MiniTweeTR</h1>';
    }
    
    /* MÃ©thode renderFooter
     *
     * Retourne le fragment HTML du bas de la page (unique pour toutes les vues)
     */
    private function renderFooter(){
        return 'La super app crÃ©Ã©e en Licence Pro &copy;2019';
    }

    /* MÃ©thode renderHome
     *
     * Vue de la fonctionalitÃ© afficher tous les Tweets. 
     *  
     */
    public function renderOneTweet(){
        return
            <<<HTML
    <body><h3> $this->rende</h3></body>
HTML;

    }

    private function renderHome(){
        return
            <<<HTML
    <body>$this->data</body>
HTML;
    }
  
    /* MÃ©thode renderUeserTweets
     *
     * Vue de la fonctionalitÃ© afficher tout les Tweets d'un utilisateur donnÃ©. 
     * 
     */
     
    private function renderUserTweets(){
        return
    <<<HTML
    <body><h3>$this->data</h3></body>
HTML;
        /* 
         * Retourne le fragment HTML pour afficher
         * tous les Tweets d'un utilisateur donnÃ©. 
         *  
         * L'attribut $this->data contient un objet User.
         *
         */
        
    }
  
    /* MÃ©thode renderViewTweet 
     * 
     * RrÃ©alise la vue de la fonctionnalitÃ© affichage d'un tweet
     *
     */
    
    private function renderViewTweet(){
        return <<<HTML
    <body><h3>$this->data</h3></body>
HTML;
        /* 
         * Retourne le fragment HTML qui rÃ©alise l'affichage d'un tweet 
         * en particuliÃ© 
         * 
         * L'attribut $this->data contient un objet Tweet
         *
         */
        
    }

    /* MÃ©thode renderPostTweet
     *
     * Realise la vue de rÃ©gider un Tweet
     *
     */
    protected function renderPostTweet(){
        return <<<HTML
    <body>
<form action="/post/" method="post">
        <label for="msg">Message :</label>
        <textarea id="msg" name="user_message"></textarea>
</form> </body>
HTML;
        /* MÃ©thode renderPostTweet
         *
         * Retourne la framgment HTML qui dessine un formulaire pour la rÃ©daction 
         * d'un tweet, l'action (bouton de validation) du formulaire est la route "/send/"
         *
         */
        
    }


    /* MÃ©thode renderBody
     *
     * Retourne la framgment HTML de la balise <body> elle est appelÃ©e
     * par la mÃ©thode hÃ©ritÃ©e render.
     *
     */
    public function renderBody($selector){
        switch ($selector) {
            case "renderViewTweet":
               $body = $this->renderViewTweet();
                break;
            case "renderUserTweets":
                $body = $this->renderUserTweets();
                break;
            case "renderHome":
                $body = $this->renderHome();
                break;
            case "renderPostTweet":
                $body = $this->renderPostTweet();
                break;
        }
        return <<<HTML
       <section>
          $body;
       </section>
HTML;

    }
    public function render($selector){
        return <<<HTML
        <!DOCTYPE html>
<html lang="fr">
   <head>
      <meta charset="utf-8">
      <title> DEDE </title>
      <link rel="stylesheet" href="html/style.css">
   </head>
   <body>
       <header> {$this->renderHeader()}</header>
       <section>
          {$this->renderBody($selector)}
       </section>
       <footer> {$this->renderFooter()} </footer>
   </body>
</html>
HTML;
    }

}