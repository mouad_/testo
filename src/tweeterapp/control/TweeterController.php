<?php
namespace tweeterapp\control;
use tweeterapp\model\Tweet as Tweet;
use tweeterapp\model\User as User;
use tweeterapp\view\TweeterView as TweeterView;

/* Classe TweeterController :
 *  
 * RÃ©alise les algorithmes des fonctionnalitÃ©s suivantes: 
 *
 *  - afficher la liste des Tweets 
 *  - afficher un Tweet
 *  - afficher les tweet d'un utilisateur 
 *  - afficher la le formulaire pour poster un Tweet
 *  - afficher la liste des utilisateurs suivis 
 *  - Ã©valuer un Tweet
 *  - suivre un utilisateur
 *   
 */

class TweeterController extends \mf\control\AbstractController {


    public function __construct(){
        parent::__construct();
    }

    /* affiche tous les tweets */
    public function viewHome(){
        $requeteTweetSup1 = Tweet::select("author","text","created_at")
                            ->orderBY("created_at");
        $uniq = $requeteTweetSup1->get();
        $TweeterView = new TweeterView($uniq);
        echo $TweeterView->render("renderHome");
    }

    /* affiche le tweet passer en parametre */
    public function viewTweet($idTweet){
        $tweet  = Tweet::where('id', '=', $idTweet)->first();   
        $tweet  = $tweet->tweets()->get();
        $TweeterView = new TweeterView($tweet);
        echo $TweeterView->render("renderViewTweet");
    }    

  /* affiche les users qui ont appréciés le Tweet */
    public function likedBy($idTweet){
        $tweet = Tweet::where("id" , "=", $idTweet)->first();
        $user  = $tweet->likedBy()->get();
        echo $user;
    }

    /* affiche l'auteur du tweet passer en parametre*/
    public function author($idTweet){
        $user  = Tweet::where('id', '=', $idTweet)->first();
        $tweet = $user->author()->get();
        foreach ($tweet as $k) {
            echo "written by user ◦"." ".$k->username;
            echo "<br>";
        }
    }

/* affiche les tweets de l'user passer en parametre */
    public function viewUserTweets($idUser){
        $user  = User::where('id', '=', $idUser)->first();
        $tweet = $user->tweets()->get();
        $TweeterView = new TweeterView($tweet);
        echo $TweeterView->render("renderUserTweets");
    }


    /* affiche les tweets de l'user passer en parametre */
    public function tweeterForm(){
        $TweeterView = new TweeterView();
        echo $TweeterView->render("renderPostTweet");
    }

}
