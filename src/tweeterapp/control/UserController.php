<?php
namespace tweeterapp\control;
use tweeterapp\model\Tweet as Tweet;
use tweeterapp\model\User as User;

class UserController extends \mf\control\AbstractController {
	
	public function __construct(){
		parent::__construct();
	}
	
	/* permet de supprimer un user */
	public function DeleteUser($iduser){
        $userBeta = User::find($iduser);
		$userBeta->delete(); 
        }

	/* affiche les tweets liker par un utilisateur et l'utilisateur */
    public function liked($iduser){
    	$u = User::where('id' ,'=', $iduser)->first();
		$tweetLiked = $u->liked()->get();
		echo $tweetLiked;
        }

        /* les utilisateurs qui suivent l'auteur passer en parametre */
    public function followedBy($iduser){
    	$u = User::where('id' ,'=', $iduser)->first();
		$tweetLiked = $u->followedBy()->get();
		echo $tweetLiked;  
        }


        /* les utilisateurs suivis par l'auteur passer en parametre */
    public function follows($iduser){
    	$u = User::where('id' ,'=', $iduser)->first();
		$tweetLiked = $u->follows()->get();
		echo $tweetLiked;  
        }

    }