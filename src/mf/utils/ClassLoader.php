<?php
namespace mf\utils;

Class ClassLoader {
private $prefix;

public function __construct($prefix){
    	$this->prefix = $prefix;
    }

public function loadClass($class){
$test_replace = str_replace('\\',DIRECTORY_SEPARATOR,$class);
$filepath = $this->prefix.DIRECTORY_SEPARATOR.$test_replace.'.php';
	if (file_exists($filepath)) {
		require_once($filepath);
	}
}

public function register(){
	spl_autoload_register(array($this,'loadClass'));
    }
}