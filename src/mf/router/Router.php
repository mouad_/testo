<?php 
namespace mf\router;

class Router extends AbstractRouter{

	protected $http_req = null;

	public function __construct(){
		parent::__construct();
	}

	public function addRoute($name, $url, $ctrl, $mth){
		self::$routes[$url] = array($ctrl,$mth);
		self::$aliases[$name] = $url;
		var_dump(self::$routes[$url]);
	}

	public function setDefaultRoute($url){
		self::$aliases['default'] = $url;
	}

	public function run(){
		$route = self::$aliases['default'];
		$ctrl = self::$routes[$route][0];
		$mth  = self::$routes[$route][1];
		if (array_key_exists($this->http_req->path_info, self::$routes)){
			$ctrl = self::$routes[$this->http_req->path_info][0];
			$mth = self::$routes[$this->http_req->path_info][1];
		}
		$controller = new $ctrl();
		$controller->$mth();
	}

	public function executeRoute($alias){

		$route = self::$aliases[$alias];
		$ctrl = self::$routes[$route][0];
		$mth = self::$routes[$route][1];
		if (array_key_exists($alias, self::$aliases)){
			$ctrl = self::$routes[$route][0];
			$mth = self::$routes[$route][1];
		}
		$controller = new $ctrl();
		$controller->$mth();
	}

	public function urlFor($route_name, $param_list=[]){

	}
}
