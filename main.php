<?php
/* pour le chargement automatique des classes d'Eloquent (dans le répertoire vendor) */
require_once 'vendor/autoload.php';
require_once 'src/mf/utils/ClassLoader.php';
use mf\utils\ClassLoader as ClassLoader;
use mf\utils\AbstractHttpRequest as AbstractHttpRequest;
use mf\utils\HttpRequest as HttpRequest;
use mf\router\Router as router;
use tweeterapp\control\TweeterController as TweeterController;
use tweeterapp\control\UserController as UserController;
use tweeterapp\view\TweeterView as TweeterView;
$config   = parse_ini_file("conf/config.ini");
$classloader = new ClassLoader("src");
$classloader->register();
$db = new Illuminate\Database\Capsule\Manager();
$db->addConnection( $config ); /* configuration avec nos paramètres */
$db->setAsGlobal();            /* rendre la connexion visible dans tout le projet */
$db->bootEloquent();           /* établir la connexion */
$router = new router();
$router->addRoute('maison', '/home/','\tweeterapp\control\TweeterController','viewHome');
$router->addRoute('dede','/dede/','\tweeterapp\control\TweeterController','viewHome');
$router->setDefaultRoute('/home/');
$router->run();


//$ctrlU = new UserController();
//$ctrl->viewHome();
//echo $ctrl->viewHome();
//echo $ctrl->viewTweet(58);
//print_r(Router::$routes);
//print_r(Router::$aliases);
//echo $ctrl->author(50);
//echo $ctrl->viewUserTweets(8);
//echo $ctrlU->liked(10);
//echo $ctrl->tweetAuthor();